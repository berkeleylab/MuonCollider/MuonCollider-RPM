Specifications required to produce unofficial RPM's of packages required for the Muon Collider software framework. Most are updated versions of packages required by ACTS.

## Packages
- Eigen3 3.3.7
- Boost 1.73 (parallel install under boost173)
